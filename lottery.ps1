$seed = Get-Date -UFormat %s

$itemArray = @(
    "リンゴ",
    "バナナ",
    "キウイ",
    "イチゴ",
    "ブドウ"
)

Get-Random -InputObject $itemArray -SetSeed $seed

